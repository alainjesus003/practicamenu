
package menu;
public class PracticaRecibo {

    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Verificando errores");
        dbRecibo rec = new dbRecibo();
        rec.conectar();
        
        Recibo recibo = new Recibo();
        recibo.setNumRecibo(100);
        recibo.setNombre("miguel jose");
        recibo.setDomicilio("av luna 33");
        recibo.setCosto(10000f);
        recibo.setConsumo(1112f);
        recibo.setTipo(1);
        recibo.setFecha("2023-12-08");
        
        rec.actualizar(recibo);
        
        rec.deshabilitar(recibo);
        if(rec.isExiste(100, 0)) System.out.println("si existe");
        else System.out.println("no existe");
        rec.habilitar(recibo);
        if(rec.isExiste(100, 0)) System.out.println("si existe");
        else System.out.println("no existe");
        
        Recibo result= rec.buscar(100);
        
        if(result.getId()>0){
            System.out.println(result.getNombre() +"dom"+result.getDomicilio());
        }
    }
    
}